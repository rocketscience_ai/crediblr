## README

### Connect to AWS EC2

```sh
ssh -i ~/.ssh/aws-altasapiens.pem ubuntu@52.11.180.95
```

add swap to the instance http://stackoverflow.com/questions/17173972/how-do-you-add-swap-to-an-ec2-instance

### Install package on AWS EC2

```sh
sudo R
```

then

```r
devtools::install_bitbucket("redmode/crediblr", password = "<PASSWORD>")
```

### Curl call

```sh
curl http://52.11.180.95/ocpu/library/crediblR/R/FacebookUser/json -d "access_token='<TOKEN>'"
```

### R Call

```r
library(httr)
library(stringi)
library(jsonlite)

access_token <- "<TOKEN>"
base.url <- "http://52.11.180.95/ocpu"

query <- POST(url = stri_c(base.url, "/library/crediblR/R/FacebookUser"),
              body = list(access_token = access_token), 
              encode = "json")
              
result <- stri_split_fixed(content(query), "\n")[[1]][1]

user <- GET(url = stri_c(base.url, "/..", result, "/json"))
user <- fromJSON(content(user, as = "text"))
user
```

### Asynchonous R call

```r
library(httr)
library(stringi)
library(jsonlite)

access_token <- "<TOKEN>"
base.url <- "http://52.11.180.95/ocpu"

# Query #1 - Get ID
query <- POST(url = stri_c(base.url, "/library/crediblR/R/CreditScore"),
              body = list(access_token = access_token), 
              encode = "json")

result <- stri_split_fixed(content(query), "\n")[[1]][1]

id <- GET(url = stri_c(base.url, "/..", result, "/json"))
id <- fromJSON(content(id, as = "text"))
id

# Query #2 - Get answer (~20 sec delay)
query <- POST(url = stri_c(base.url, "/library/crediblR/R/GetResults"),
              body = list(id = id), 
              encode = "json")

result <- stri_split_fixed(content(query), "\n")[[1]][1]

user <- GET(url = stri_c(base.url, "/..", result, "/json"))
user <- fromJSON(content(user, as = "text"))
user
```
